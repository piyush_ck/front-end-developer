# Front End Developer Practical Test



## Getting started

Hello Candidate, 

Welcome to Cupidknot Practical Exam.


Front End Design & Development Assessment

# *Note: Use HTML, CSS(bootstrap) to design and develop below page with responsive UI.
Page Design link: [Click here](https://www.figma.com/proto/uhbBJhZ6JeLBAL6qILoFEH/FrontEnd-Developer-Assignment?page-id=0%3A1&node-id=1%3A51&viewport=519%2C599%2C0.26&scaling=min-zoom)

Instructions for assesment :-

* Please make the exact copy of the demo page provided.
* Create a simple continuous animation anywhere in the page using CSS.
* Try to make the design page using bootstrap only and minimize the use of custom CSS as much as possible.
* For icons use the font awesome icon library.


IMP NOTES:

- After completing the task create public repository in GitHub or Gitlab or Bitbucket and push the code.
- If you don't know about git then you can send zip in mail attachment.
- Code should be well formatted and follows standard guidelines


